package um.practicas.metaenlace;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.ApplicationListener;

import org.springframework.context.event.ContextRefreshedEvent;

import org.springframework.stereotype.Component;

import um.practicas.metaenlace.entidades.*;
import um.practicas.metaenlace.repositorios.*;

@Component
public class InsertarDatos implements ApplicationListener<ContextRefreshedEvent> {
	
	@Autowired
	private IMedicoRepo medicoRepo;
	
	@Autowired
	private IPacienteRepo pacienteRepo;
	
	@Autowired
	private ICitaRepo citaRepo;
	
	@Autowired
	private IDiagnosticoRepo diagRepo;
	


	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {

		Paciente paciente1 = new Paciente("Pedro", "Perez", "Pedrez", "clave1", "NSS1234", "5748", "644888999", "direccion1");
		Paciente paciente2 = new Paciente("Pepe", "Martinez", "pepe1", "clave2", "NSS1568", "8349", "694333212", "direccion2");
		Paciente paciente3 = new Paciente("Julio", "Perez", "jjjj", "clave3", "NSS1234", "5748", "693478112", "direccion3");
		
		Medico medico1 = new Medico("Pedro", "Medicina", "Medico1", "clave4", "C12312");
		Medico medico2 = new Medico("Juan", "Medicino", "Medico2", "clave5", "C12643");
		
		paciente1.addMedico(medico1);
		paciente2.addMedico(medico1);
		medico1.addPaciente(paciente1);
		medico1.addPaciente(paciente2);

		paciente3.addMedico(medico2);
		medico2.addPaciente(paciente3);
		
		
		Cita c1 = new Cita(new Date(), "dolor de cabeza", medico1, paciente1);
		Diagnostico d1 = new Diagnostico("Enfermedad leve", "Migraña", c1);
		c1.setDiagnostico(d1);
		paciente1.addCita(c1);
		medico1.addCita(c1);
		
		
		Cita c2 = new Cita(new Date(), "vomitos", medico1, paciente2);
		Diagnostico d2 = new Diagnostico("Deshidratacion", "Gastroenteritis", c2);
		c2.setDiagnostico(d2);
		paciente2.addCita(c2);
		medico1.addCita(c2);
		
		Cita c3 = new Cita(new Date(), "dificultad respiratoria", medico2, paciente3);
		//Diagnostico d3 = new Diagnostico("Estado grave", "Covid-19", c3);
		//c3.setDiagnostico(d3);
		paciente3.addCita(c3);
		medico2.addCita(c3);
		
		
		pacienteRepo.save(paciente1);
		pacienteRepo.save(paciente2);
		pacienteRepo.save(paciente3);
		
/*		medicoRepo.save(medico1);
		medicoRepo.save(medico2);
		
		citaRepo.save(c1);
		citaRepo.save(c2);
		citaRepo.save(c3);
		
		diagRepo.save(d1);
		diagRepo.save(d2);
		diagRepo.save(d3);
*/
		
		
		
		
		
	}

}