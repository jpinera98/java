package um.practicas.metaenlace.dto;

import java.io.Serializable;

public class CitaDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idCita;
	private String fechaHora;
	private String motivoCita;
	private String medicoIdUsuario;
	private String pacienteIdUsuario;
	private String diagnosticoIdDiagnostico;
	
	public String getIdCita() {
		return idCita;
	}
	public void setIdCita(String idCita) {
		this.idCita = idCita;
	}
	public String getMotivoCita() {
		return motivoCita;
	}
	public void setMotivoCita(String motivoCita) {
		this.motivoCita = motivoCita;
	}
	public String getMedicoIdUsuario() {
		return medicoIdUsuario;
	}
	public void setMedicoIdUsuario(String medicoIdUsuario) {
		this.medicoIdUsuario = medicoIdUsuario;
	}
	public String getPacienteIdUsuario() {
		return pacienteIdUsuario;
	}
	public void setPacienteIdUsuario(String pacienteIdUsuario) {
		this.pacienteIdUsuario = pacienteIdUsuario;
	}
	public String getDiagnosticoIdDiagnostico() {
		return diagnosticoIdDiagnostico;
	}
	public void setDiagnosticoIdDiagnostico(String diagnosticoIdDiagnostico) {
		this.diagnosticoIdDiagnostico = diagnosticoIdDiagnostico;
	}
	public String getFechaHora() {
		return fechaHora;
	}
	public void setFechaHora(String fechaHora) {
		this.fechaHora = fechaHora;
	}
	
	
	
	
}
