package um.practicas.metaenlace.dto;

import java.io.Serializable;

public class DiagnosticoDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String idDiagnostico;
	private String valoracionEspecialista;
	private String enfermedad;
	private String citaIdCita;
	
	public String getIdDiagnostico() {
		return idDiagnostico;
	}
	public void setIdDiagnostico(String idDiagnostico) {
		this.idDiagnostico = idDiagnostico;
	}
	public String getValoracionEspecialista() {
		return valoracionEspecialista;
	}
	public void setValoracionEspecialista(String valoracionEspecialista) {
		this.valoracionEspecialista = valoracionEspecialista;
	}
	public String getEnfermedad() {
		return enfermedad;
	}
	public void setEnfermedad(String enfermedad) {
		this.enfermedad = enfermedad;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getCitaIdCita() {
		return citaIdCita;
	}
	public void setCitaIdCita(String citaIdCita) {
		this.citaIdCita = citaIdCita;
	}
	
	
}
