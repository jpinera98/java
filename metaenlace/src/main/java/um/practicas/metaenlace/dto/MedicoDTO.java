package um.practicas.metaenlace.dto;

import java.io.Serializable;
import java.util.*;

import um.practicas.metaenlace.entidades.Cita;

public class MedicoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idUsuario;
	private String nombre;
	private String apellidos;
	private String usuario;
	private String clave;
	private String numColegiado;
	private List<String> citas;
	private List<String> pacientes;
	

	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getNumColegiado() {
		return numColegiado;
	}
	public void setNumColegiado(String numColegiado) {
		this.numColegiado = numColegiado;
	}
	public List<String> getCitas() {
		return citas;
	}
	public void setCitas(List<String> list) {
		this.citas = list;
	}
	public List<String> getPacientes() {
		return pacientes;
	}
	public void setPacientes(List<String> pacientes) {
		this.pacientes = pacientes;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	
}
