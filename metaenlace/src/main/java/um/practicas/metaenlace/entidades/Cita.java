package um.practicas.metaenlace.entidades;

import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "citas")
public class Cita{
	
	@Id
	@GeneratedValue( strategy = GenerationType.SEQUENCE, generator= "seq_cita" )
	@SequenceGenerator(name = "seq_cita", sequenceName = "seq_cita", allocationSize=1)
	private int idCita;
	
	@Column(name = "fecha_hora")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaHora;
	@Column(name = "motivo_cita")
	private String motivoCita;
	
	@ManyToOne
	@JoinColumn(name = "id_medico")
	private Medico medico;
	
	@ManyToOne
	@JoinColumn(name = "id_paciente")
	private Paciente paciente;
	
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "diagnostico_id", referencedColumnName = "idDiagnostico", nullable = true)
	private Diagnostico diagnostico;

	public Cita() {
		super();
	}

	public Cita(Date fechaHora, String motivoCita, Medico medico, Paciente paciente) {
		this.fechaHora = fechaHora;
		this.motivoCita = motivoCita;
		this.paciente = paciente;
		this.medico = medico;
	}

	public Date getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}

	public String getMotivoCita() {
		return motivoCita;
	}

	public void setMotivoCita(String motivoCita) {
		this.motivoCita = motivoCita;
	}
	
	public Diagnostico getDiagnostico() {
		return this.diagnostico;
	}
	
	public void setDiagnostico(Diagnostico diagnostico) {
		this.diagnostico = diagnostico;
	}
	
	public Medico getMedico() {
		return this.medico;
	}
	
	public Paciente getPaciente() {
		return this.paciente;
	}
	
	public void setMedico(Medico medico) {
		this.medico = medico;
	}
	
	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public Integer getIdCita() {
		return idCita;
	}

	public void setIdCita(int idCita) {
		this.idCita = idCita;
	}
	
	

}
