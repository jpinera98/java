package um.practicas.metaenlace.entidades;



import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "diagnosticos")
public class Diagnostico{
	
	@Id
	@GeneratedValue( strategy = GenerationType.SEQUENCE, generator= "seq_diagnostico" )
	@SequenceGenerator(name = "seq_diagnostico", sequenceName = "seq_diagnostico", allocationSize=1)	
	private int idDiagnostico;
	
	
	@Column(name = "val_especialista")
	private String valoracionEspecialista;
	
	@Column(name = "enfermedad")
	private String enfermedad;
	
	@OneToOne(mappedBy = "diagnostico", cascade = CascadeType.PERSIST)
	private Cita cita;

	public Diagnostico() {
		super();
	}

	public Diagnostico(String valoracionEspecialista, String enfermedad, Cita cita) {
		this.valoracionEspecialista = valoracionEspecialista;
		this.enfermedad = enfermedad;
		this.cita = cita;
	}

	public String getValoracionEspecialista() {
		return valoracionEspecialista;
	}

	public void setValoracionEspecialista(String valoracionEspecialista) {
		this.valoracionEspecialista = valoracionEspecialista;
	}

	public String getEnfermedad() {
		return enfermedad;
	}

	public void setEnfermedad(String enfermedad) {
		this.enfermedad = enfermedad;
	}

	public int getIdDiagnostico() {
		return idDiagnostico;
	}

	public void setIdDiagnostico(int idDiagnostico) {
		this.idDiagnostico = idDiagnostico;
	}

	public Cita getCita() {
		return cita;
	}

	public void setCita(Cita cita) {
		this.cita = cita;
	}
	
	

}
