package um.practicas.metaenlace.entidades;

import java.util.*;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@PrimaryKeyJoinColumn(referencedColumnName = "idUsuario")
@Table(name = "medicos")

public class Medico extends Usuario{
	
	
	@Column(name = "num_colegiado")
	private	String numColegiado;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="medico")
	private List<Cita> citas;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "medico_paciente",
				joinColumns = {@JoinColumn(name = "id_medico")},
				inverseJoinColumns = {@JoinColumn(name = "id_paciente")})
	private List<Paciente> pacientes;
	
	public Medico() {
		super();
	}

	public Medico(String nombre, String apellidos, String usuario, String clave, String numColegiado) {
		super(nombre, apellidos, usuario, clave);
		this.numColegiado = numColegiado;
		this.citas = new LinkedList<Cita>();
		this.pacientes = new LinkedList<Paciente>();
	}

	public String getNumColegiado() {
		return numColegiado;
	}

	public void setNumColegiado(String numColegiado) {
		this.numColegiado = numColegiado;
	}
	
	public void addCita(Cita cita) {
		this.citas.add(cita);
	}
	
	public void addPaciente(Paciente paciente) {
		this.pacientes.add(paciente);
	}
	
	public List<Cita> getCitas() {
		return citas;
	}

	public void setCitas(List<Cita> citas) {
		this.citas = citas;
	}

	public List<Paciente> getPacientes() {
		return pacientes;
	}

	public void setPacientes(List<Paciente> pacientes) {
		this.pacientes = pacientes;
	}

}
