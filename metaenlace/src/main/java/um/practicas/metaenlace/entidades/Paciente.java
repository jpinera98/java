package um.practicas.metaenlace.entidades;

import java.util.*;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@PrimaryKeyJoinColumn(referencedColumnName = "idUsuario")
@Table(name = "pacientes")

public class Paciente extends Usuario{

	@Column(name = "nss")
	private String nss;
	@Column(name = "num_tarjeta")
	private String numTarjeta;
	@Column(name = "telefono")
	private String telefono;
	@Column(name = "direccion")
	private String direccion;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="paciente")
	private Set<Cita> citas;
	
	@ManyToMany(mappedBy = "pacientes", cascade = CascadeType.ALL)
	private List<Medico> medicos;
	

	public Paciente() {
		super();
	}

	public Paciente(String nombre, String apellidos, String usuario, String clave, String nss, String numTarjeta, String telefono, String direccion) {
		super(nombre, apellidos, usuario, clave);
		this.nss = nss;
		this.numTarjeta = numTarjeta;
		this.telefono = telefono;
		this.direccion = direccion;
		this.citas = new HashSet<Cita>();
		this.medicos = new LinkedList<Medico>();
	}

	public String getNss() {
		return nss;
	}

	public void setNss(String nss) {
		this.nss = nss;
	}

	public String getNumTarjeta() {
		return numTarjeta;
	}

	public void setNumTarjeta(String numTarjeta) {
		this.numTarjeta = numTarjeta;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	public void addCita(Cita cita) {
		this.citas.add(cita);
	}
	
	public void addMedico(Medico medico) {
		this.medicos.add(medico);
	}

	public Set<Cita> getCitas() {
		return citas;
	}

	public void setCitas(Set<Cita> citas) {
		this.citas = citas;
	}

	public List<Medico> getMedicos() {
		return medicos;
	}

	public void setMedicos(List<Medico> medicos) {
		this.medicos = medicos;
	}
	
}
