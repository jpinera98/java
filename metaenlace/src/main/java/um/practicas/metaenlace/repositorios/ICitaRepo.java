package um.practicas.metaenlace.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;

import um.practicas.metaenlace.entidades.Cita;


public interface ICitaRepo extends JpaRepository<Cita, Integer>{

}
