package um.practicas.metaenlace.repositorios;


import org.springframework.data.jpa.repository.JpaRepository;

import um.practicas.metaenlace.entidades.Diagnostico;

public interface IDiagnosticoRepo extends JpaRepository<Diagnostico, Integer>{

}
