package um.practicas.metaenlace.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import um.practicas.metaenlace.entidades.Medico;

@Repository
public interface IMedicoRepo extends JpaRepository<Medico, Integer>{

}
