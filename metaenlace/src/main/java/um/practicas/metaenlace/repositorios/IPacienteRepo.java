package um.practicas.metaenlace.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import um.practicas.metaenlace.entidades.Paciente;

@Repository
public interface IPacienteRepo extends JpaRepository<Paciente, Integer>{

}
