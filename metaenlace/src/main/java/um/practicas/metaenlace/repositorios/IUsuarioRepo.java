package um.practicas.metaenlace.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import um.practicas.metaenlace.entidades.Usuario;

@Repository
public interface IUsuarioRepo extends JpaRepository<Usuario, Integer> {

}
