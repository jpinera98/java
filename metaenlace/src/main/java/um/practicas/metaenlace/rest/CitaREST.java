package um.practicas.metaenlace.rest;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import um.practicas.metaenlace.dto.CitaDTO;
import um.practicas.metaenlace.dto.MedicoDTO;
import um.practicas.metaenlace.entidades.Cita;
import um.practicas.metaenlace.entidades.Medico;
import um.practicas.metaenlace.servicios.CitaService;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/citas")
public class CitaREST {

	@Autowired
	private CitaService service;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@GetMapping
	public ResponseEntity<List<CitaDTO>> listAll(){
		List<CitaDTO> citaDTOs = service.listAll().stream().map(this::convertToDTO)
				.collect(Collectors.toList());
		return ResponseEntity.ok(citaDTOs);
	}
	
	@RequestMapping(value= "{idCita}")
	public ResponseEntity<CitaDTO> getMedicoById(@PathVariable("idCita") Integer idCita){
		Cita cita= service.get(idCita);
		if(cita != null) {
			return ResponseEntity.ok(convertToDTO(cita));
		}
		return ResponseEntity.noContent().build();
	}	
	
	@PostMapping
	public ResponseEntity<String> insert(@RequestBody CitaDTO citaDTO) {
		if(service.insert(convertToEntity(citaDTO))) {
			return ResponseEntity.ok("Cita insertada");
		}
		return ResponseEntity.status(409).build();
	}
	
	@PutMapping
	public void update(@RequestBody CitaDTO citaDTO) {
		service.update(convertToEntity(citaDTO));
	}
	
	@DeleteMapping(value = "/{idCita}")
	public ResponseEntity<Void> delete(@PathVariable("idCita") Integer id) {
		service.delete(id);
		return ResponseEntity.ok(null);
	}

	private CitaDTO convertToDTO(Cita cita) {
		CitaDTO citaDto = modelMapper.map(cita, CitaDTO.class);
		return citaDto;
	}
	
	private Cita convertToEntity(CitaDTO citaDTO){
		Cita cita = modelMapper.map(citaDTO, Cita.class);
		return cita;
	}
	
}
