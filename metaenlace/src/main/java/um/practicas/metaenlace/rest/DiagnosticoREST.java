package um.practicas.metaenlace.rest;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import um.practicas.metaenlace.servicios.DiagnosticoService;
import um.practicas.metaenlace.dto.DiagnosticoDTO;
import um.practicas.metaenlace.dto.MedicoDTO;
import um.practicas.metaenlace.entidades.*;
import java.util.*;
import java.util.stream.Collectors;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("diagnosticos")
public class DiagnosticoREST {

	@Autowired
	private DiagnosticoService service;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@GetMapping
	public ResponseEntity<List<DiagnosticoDTO>> listAll(){
		List<DiagnosticoDTO> medicoDTOs = service.listAll().stream().map(this::convertToDTO)
                .collect(Collectors.toList());
		return ResponseEntity.ok(medicoDTOs);
	}

	@RequestMapping(value= "{idDiagnostico}")
	public ResponseEntity<DiagnosticoDTO> getMedicoById(@PathVariable("idDiagnostico") Integer idDiagnostico){
		Diagnostico diagnostico= service.get(idDiagnostico);
		if(diagnostico != null) {
			return ResponseEntity.ok(convertToDTO(diagnostico));
		}
		return ResponseEntity.noContent().build();
	}
	
	@PostMapping
	public ResponseEntity<String> insert(@RequestBody DiagnosticoDTO diagnosticoDTO) {
		if(service.insert(convertToEntity(diagnosticoDTO))) {
			return ResponseEntity.ok("Diagnostico insertado");
		}
		return ResponseEntity.status(409).build();
	}
	
	@PutMapping
	public void update(@RequestBody DiagnosticoDTO diagnosticoDTO) {
		service.update(convertToEntity(diagnosticoDTO));
	}
	
	@DeleteMapping(value = "/{idDiagnostico}")
	public ResponseEntity<Void> delete(@PathVariable("idDiagnostico") Integer id) {
		service.delete(id);
		return ResponseEntity.ok(null);
	}
	
	private DiagnosticoDTO convertToDTO(Diagnostico diagnostico) {
		DiagnosticoDTO diagnosticoDto = modelMapper.map(diagnostico, DiagnosticoDTO.class);
		return diagnosticoDto;
	}	
	
	private Diagnostico convertToEntity(DiagnosticoDTO diagnosticoDTO){
		Diagnostico diagnostico = modelMapper.map(diagnosticoDTO, Diagnostico.class);
		return diagnostico;
	}
	
}
