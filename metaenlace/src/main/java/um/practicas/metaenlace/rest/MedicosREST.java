package um.practicas.metaenlace.rest;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import um.practicas.metaenlace.dto.MedicoDTO;
import um.practicas.metaenlace.entidades.Medico;
import um.practicas.metaenlace.entidades.Paciente;
import um.practicas.metaenlace.servicios.MedicoService;
import um.practicas.metaenlace.servicios.PacienteService;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/medicos")
public class MedicosREST {
	
	@Autowired
	private MedicoService service;
	
	@Autowired
	private ModelMapper modelMapper;

	@GetMapping
	public ResponseEntity<List<MedicoDTO>> getMedicos(){
		List<MedicoDTO> medicoDTOs = service.listAll().stream().map(this::convertToDTO)
                .collect(Collectors.toList());
		return ResponseEntity.ok(medicoDTOs);
	}
	
	@RequestMapping(value= "{idMedico}")
	public ResponseEntity<MedicoDTO> getMedicoById(@PathVariable("idMedico") Integer idMedico){
		Medico medico= service.get(idMedico);
		if(medico != null) {
			return ResponseEntity.ok(convertToDTO(medico));
		}
		return ResponseEntity.noContent().build();
	}
	
	@PostMapping
	public ResponseEntity<String> insert(@RequestBody MedicoDTO medicoDTO) {
		
		if(service.insert(convertToEntity(medicoDTO), medicoDTO.getPacientes())) {
			return ResponseEntity.ok("Medico insertado correctamente");
		}
		return ResponseEntity.status(409).build();
	}
	
	@PutMapping
	public ResponseEntity<String> update(@RequestBody MedicoDTO medicoDTO) {
		if(service.update(convertToEntity(medicoDTO))) {
			return ResponseEntity.ok("Medico actualizado correctamente");
		}
		return ResponseEntity.status(409).build();
	}
	
	@DeleteMapping(value = "/{idMedico}")
	public ResponseEntity<Void> delete(@PathVariable("idMedico") Integer id) {
		service.delete(id);
		return ResponseEntity.ok(null);
	}
	
	private MedicoDTO convertToDTO(Medico medico) {
		MedicoDTO medicoDto = modelMapper.map(medico, MedicoDTO.class);
		medicoDto.setCitas(medico.getCitas().stream()
					.map(c -> c.getIdCita().toString())
					.collect(Collectors.toList())
				);
		medicoDto.setPacientes(medico.getPacientes().stream()
					.map(p -> p.getIdUsuario().toString())
					.collect(Collectors.toList())
				);
		return medicoDto;
	}
	
	private Medico convertToEntity(MedicoDTO medicoDTO){
		Medico medico = modelMapper.map(medicoDTO, Medico.class);
		
		/*
		List<Paciente> pacientes = new LinkedList<Paciente>();
		
		for (Integer paciente : medicoDTO.getPacientes()) {
			pacientes.add(pservice.get(paciente));
		}
		
		medico.setPacientes(pacientes);
		*/
		
		return medico;
	}
	
	
}
