package um.practicas.metaenlace.rest;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import um.practicas.metaenlace.dto.PacienteDTO;
import um.practicas.metaenlace.entidades.Paciente;
import um.practicas.metaenlace.servicios.PacienteService;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("pacientes")
public class PacienteREST {

	@Autowired
	private PacienteService service;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@GetMapping
	public ResponseEntity<List<PacienteDTO>> listAll(){
		List<PacienteDTO> pacienteDTOs = service.listAll().stream().map(this::convertToDTO)
				.collect(Collectors.toList());
		return ResponseEntity.ok(pacienteDTOs);
	}
	
	@RequestMapping(value= "{idPaciente}")
	public ResponseEntity<PacienteDTO> getMedicoById(@PathVariable("idPaciente") Integer idPaciente){
		Paciente paciente= service.get(idPaciente);
		if(paciente != null) {
			return ResponseEntity.ok(convertToDTO(paciente));
		}
		return ResponseEntity.noContent().build();
	}	
	
	@PostMapping
	public ResponseEntity<String> insert(@RequestBody PacienteDTO pacienteDTO) {
		if(service.insert(convertToEntity(pacienteDTO),pacienteDTO.getMedicosIdMedico()) ) {
			return ResponseEntity.ok("Paciente insertado");
		}
		return ResponseEntity.status(409).build();
	}
	
	@PostMapping("/login")
	public ResponseEntity<PacienteDTO> login(@RequestBody PacienteDTO pacienteDTO){
		Paciente pac;
		pac = service.listAll().stream().filter(p -> p.getUsuario().equals(pacienteDTO.getUsuario()) && p.getClave().equals(pacienteDTO.getClave())).findAny().orElse(null);
		if( pac != null) {
			return ResponseEntity.ok(convertToDTO(pac));
		}
		return ResponseEntity.status(409).build();
	}
	
	
	
	@PutMapping
	public ResponseEntity<String> update(@RequestBody PacienteDTO pacienteDTO) {
		
		if(service.update(convertToEntity(pacienteDTO))) {
			return ResponseEntity.ok("Paciente actualizado correctamente");
		}
		return ResponseEntity.status(409).build();
	}
	
	@DeleteMapping(value = "/{idPaciente}")
	public ResponseEntity<Void> delete(@PathVariable("idPaciente") Integer id) {
		service.delete(id);
		return ResponseEntity.ok(null);
	}
	
	private PacienteDTO convertToDTO(Paciente paciente) {
		PacienteDTO pacienteDto = modelMapper.map(paciente, PacienteDTO.class);
		pacienteDto.setCitasIdCita(paciente.getCitas().stream()
				.map(c -> c.getIdCita().toString())
				.collect(Collectors.toList()));
		pacienteDto.setMedicosIdMedico(paciente.getMedicos().stream()
				.map(m -> m.getIdUsuario().toString())
				.collect(Collectors.toList())
				);
		return pacienteDto;
	}
	
	private Paciente convertToEntity(PacienteDTO pacienteDTO){
		Paciente paciente = modelMapper.map(pacienteDTO, Paciente.class);
		return paciente;
	}
		
}
