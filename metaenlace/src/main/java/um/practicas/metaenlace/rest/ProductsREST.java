package um.practicas.metaenlace.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class ProductsREST {

	//@RequestMapping(value = "hello", method = RequestMethod.GET)
	@GetMapping
	public String hello() {
		return "Prueba 1";
	}
}
