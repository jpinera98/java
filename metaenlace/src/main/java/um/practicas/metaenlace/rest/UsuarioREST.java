package um.practicas.metaenlace.rest;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import um.practicas.metaenlace.dto.PacienteDTO;
import um.practicas.metaenlace.dto.UsuarioDTO;
import um.practicas.metaenlace.entidades.Paciente;
import um.practicas.metaenlace.entidades.Usuario;
import um.practicas.metaenlace.servicios.UsuarioService;

@RestController
@RequestMapping("usuarios")
public class UsuarioREST {

	@Autowired
	private UsuarioService service;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@GetMapping
	public ResponseEntity<List<UsuarioDTO>> listAll(){
		List<UsuarioDTO> usuarioDTOs = service.listAll().stream().map(this::convertToDTO)
                .collect(Collectors.toList());
		return ResponseEntity.ok(usuarioDTOs);
	}
	
	@RequestMapping(value= "{idUsuario}")
	public ResponseEntity<UsuarioDTO> getMedicoById(@PathVariable("idUsuario") Integer idUsuario){
		Usuario usuario= service.get(idUsuario);
		if(usuario != null) {
			return ResponseEntity.ok(convertToDTO(usuario));
		}
		return ResponseEntity.noContent().build();
	}	
	
	@PostMapping
	public void insert(@RequestBody UsuarioDTO usuarioDTO) {
		service.insert(convertToEntity(usuarioDTO));
	}
	
	@PutMapping
	public void update(@RequestBody UsuarioDTO usuarioDTO) {
		service.update(convertToEntity(usuarioDTO));
	}
	
	@DeleteMapping(value = "/{idUsuario}")
	public ResponseEntity<Void> delete(@PathVariable("idUsuario") Integer id) {
		service.delete(id);
		return ResponseEntity.ok(null);
	}
	
	private UsuarioDTO convertToDTO(Usuario usuario) {
		UsuarioDTO usuarioDto = modelMapper.map(usuario, UsuarioDTO.class);
		return usuarioDto;
	}	
	
	private Usuario convertToEntity(UsuarioDTO usuarioDTO){
		Usuario diagnostico = modelMapper.map(usuarioDTO, Usuario.class);
		return diagnostico;
	}	
	
	
	
}
