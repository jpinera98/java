package um.practicas.metaenlace.servicios;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import um.practicas.metaenlace.entidades.Cita;
import um.practicas.metaenlace.entidades.Medico;
import um.practicas.metaenlace.entidades.Paciente;
import um.practicas.metaenlace.repositorios.ICitaRepo;
import um.practicas.metaenlace.repositorios.IMedicoRepo;
import um.practicas.metaenlace.repositorios.IPacienteRepo;

@Service
@Transactional
public class CitaService {

	@Autowired
	private ICitaRepo citaRepo;
	@Autowired
	private IMedicoRepo medicoRepo;
	@Autowired
	private IPacienteRepo pacienteRepo;
	
	public boolean insert(Cita c) {
		Medico m = medicoRepo.findById(c.getMedico().getIdUsuario()).orElse(null);
		Paciente p = pacienteRepo.findById(c.getPaciente().getIdUsuario()).orElse(null);
		
		if(m == null || p == null) {
			return false;
		}
		
		c.setMedico(m);
		c.setPaciente(p);
		p.addCita(c);
		m.addCita(c);
		
		citaRepo.save(c);
		return true;
	}
	
	public Cita get(Integer id) {
		return citaRepo.findById(id)
			.orElse(null);
	}
	
	public void update(Cita c) {
		citaRepo.save(c);
	}
	
	public void delete(Integer id) {
		citaRepo.deleteById(id);
	}
	
	public List<Cita> listAll(){
		return citaRepo.findAll();
	}

}
