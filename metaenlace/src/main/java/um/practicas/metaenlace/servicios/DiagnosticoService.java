package um.practicas.metaenlace.servicios;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import um.practicas.metaenlace.entidades.Cita;
import um.practicas.metaenlace.entidades.Diagnostico;
import um.practicas.metaenlace.repositorios.ICitaRepo;
import um.practicas.metaenlace.repositorios.IDiagnosticoRepo;

import java.util.*;

@Service
@Transactional
public class DiagnosticoService {
	
	@Autowired
	private IDiagnosticoRepo diagRepo;
	@Autowired
	private ICitaRepo citaRepo;
	
	public boolean insert(Diagnostico d) {
		Cita c = citaRepo.findById(d.getCita().getIdCita()).orElse(null);
		
		if(c==null) return false;
		
		d.setCita(c);
		c.setDiagnostico(d);
		citaRepo.save(c);
		return true;
	}
	
	public Diagnostico get(Integer id) {
		return diagRepo.findById(id)
			.orElse(null);
	}
	
	public void update(Diagnostico d) {
		diagRepo.save(d);
	}
	
	public void delete(Integer id) {
		
		diagRepo.deleteById(id);
	}
	
	public List<Diagnostico> listAll(){
		return diagRepo.findAll();
	}

}
