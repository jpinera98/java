package um.practicas.metaenlace.servicios;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import um.practicas.metaenlace.entidades.Medico;
import um.practicas.metaenlace.entidades.Paciente;
import um.practicas.metaenlace.repositorios.IMedicoRepo;
import um.practicas.metaenlace.repositorios.IPacienteRepo;

import java.util.*;

@Service
@Transactional
public class MedicoService {

		@Autowired
		private IMedicoRepo medRepo;
		@Autowired
		private IPacienteRepo pacienteRepo;
		
		public boolean insert(Medico m, List<String> pacientes) {
			
			List<Paciente> pacientesAux = new LinkedList<Paciente>();
			
			for(String paciente : pacientes) {
				Paciente pac = pacienteRepo.findById(Integer.valueOf(paciente)).orElse(null);
				
				if(pac == null) return false;
				pacientesAux.add(pac);
			}
			
			for(Paciente paciente : pacientesAux) {
				paciente.addMedico(m);
			}
			
			m.setPacientes(pacientesAux);
			medRepo.save(m);
			return true;
		}
		
		public Medico get(Integer id) {
			return medRepo.findById(id).orElse(null);
		}
		
		public boolean update(Medico m) {
			
			Medico medico = medRepo.findById(m.getIdUsuario()).orElse(null);
			if(medico == null) return false;
			
			m.setCitas(medico.getCitas());
			m.setPacientes(medico.getPacientes());
		
			medRepo.save(m);
			return true;
		}
		
		public List<Medico> listAll(){
			return medRepo.findAll();
		}
		
		public void delete(Integer id) {
			medRepo.deleteById(id);
		}
		
}
