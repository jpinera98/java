package um.practicas.metaenlace.servicios;

import java.util.LinkedList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import um.practicas.metaenlace.entidades.Medico;
import um.practicas.metaenlace.entidades.Paciente;
import um.practicas.metaenlace.repositorios.IMedicoRepo;
import um.practicas.metaenlace.repositorios.IPacienteRepo;

@Service
@Transactional
public class PacienteService {
	
	@Autowired
	private IPacienteRepo pacRepo;
	
	@Autowired
	private IMedicoRepo medRepo;
	
	public boolean insert(Paciente p, List<String> medicos) {
		List<Medico> medicosAux = new LinkedList<Medico>();
		
		if(medicos!=null) {
			for(String medico: medicos) {
				Medico med = medRepo.findById(Integer.valueOf(medico)).orElse(null);
				
				if(med == null) return false;
				medicosAux.add(med);
			}
		}

		for(Medico medico : medicosAux) {
			medico.addPaciente(p);
		}
		
		p.setMedicos(medicosAux);
		pacRepo.save(p);
		return false;
	}
	
	public Paciente get(Integer id) {
		return pacRepo.findById(id)
			.orElse(null);
	}
	
	public boolean update(Paciente p) {
		
		Paciente paciente = pacRepo.findById(p.getIdUsuario()).orElse(null);
		if(paciente == null) return false;
		
		p.setMedicos(paciente.getMedicos());
		p.setCitas(p.getCitas());
		
		pacRepo.save(p);
		return true;
	}
	
	public void delete(Integer id) {
		pacRepo.deleteById(id);
	}
	
	public List<Paciente> listAll(){
		return pacRepo.findAll();
	}

}
