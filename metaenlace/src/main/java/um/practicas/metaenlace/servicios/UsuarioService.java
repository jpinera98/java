package um.practicas.metaenlace.servicios;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import um.practicas.metaenlace.entidades.Usuario;
import um.practicas.metaenlace.repositorios.IUsuarioRepo;

@Service
@Transactional
public class UsuarioService {

	@Autowired
	private IUsuarioRepo userRepo;

	public void insert(Usuario u) {
		userRepo.save(u);
	}
	
	public Usuario get(Integer id) {
		return userRepo.findById(id)
			.orElse(null);
	}
	
	public void update(Usuario u) {
		userRepo.save(u);
	}
	
	public void delete(Integer id) {
		userRepo.deleteById(id);
	}
	
	public List<Usuario> listAll(){
		return userRepo.findAll();
	}

	
}
