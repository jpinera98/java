create sequence seq_cita start with 1 increment by  1
create sequence seq_diagnostico start with 1 increment by  1
create sequence seq_user start with 1 increment by  1

create table citas (id_cita number(10,0) not null, fecha_hora date, motivo_cita varchar2(255 char), id_medico number(10,0), id_paciente number(10,0), primary key (id_cita))
create table diagnosticos (id_diagnostico number(10,0) not null, enfermedad varchar2(255 char), val_especialista varchar2(255 char), cita_id number(10,0), primary key (id_diagnostico))
create table medicos (num_colegiado varchar2(255 char), id_user number(10,0) not null, primary key (id_user))
create table medico_paciente (id_medico number(10,0) not null, id_paciente number(10,0) not null)
create table pacientes (direccion varchar2(255 char), nss varchar2(255 char), num_tarjeta varchar2(255 char), telefono varchar2(255 char), id_user number(10,0) not null, primary key (id_user))
create table usuarios (id_user number(10,0) not null, apellidos varchar2(255 char), clave varchar2(255 char), nombre varchar2(255 char), usuario varchar2(255 char), primary key (id_user))

alter table citas add constraint FK3m4sa30jkr6uyy4krl6k7jir3 foreign key (id_medico) references medicos
alter table citas add constraint FK7fljkhue1c7r80b4li70f6fh3 foreign key (id_paciente) references pacientes
alter table diagnosticos add constraint FKfykmhdrsbp2x5r8svigpwmkxj foreign key (cita_id) references citas
alter table medicos add constraint FKn7qc8r80xbkg91jhhlb993kgl foreign key (id_user) references usuarios
alter table medico_paciente add constraint FKkpu753x3gixden9u3yuyfrso5 foreign key (id_paciente) references pacientes
alter table medico_paciente add constraint FK9u2qtknck85erdsq1efcsw1p foreign key (id_medico) references medicos
alter table pacientes add constraint FKp9f2ujs4x1740xq0588uv935o foreign key (id_user) references usuarios